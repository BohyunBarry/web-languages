<?php
require ('./model/database.php');
require ('./model/holiday_db.php');

$action = filter_input(INPUT_POST,'action');

if ($action == NULL){
    $action = filter_input(INPUT_GET,'action');

    if ($action == NULL){
        $action = 'list_holidays';
    }
}

if ($action == 'list_holidays'){
    $holidays = get_holidays();
    include('./view/holiday_list.php');
} else if ($action == 'delete_holiday'){
    $holidays_id = filter_input(INPUT_POST,'holidays_id',FILTER_VALIDATE_INT);

    //$holidays = get_holidays();
    if ($holidays_id == NULL || $holidays_id == FALSE || $id == NULL || $id == FALSE){
        $error = "Missing or Incorrect holiday id.";
        include('./errors/error.php');
    } else{
        delete_holiday($holidays_id);
        header("Location: .?holidays_id=$holidays_id");

    }
} else if ($action == 'show_add_form'){
    include('./view/holiday_add.php');
} else if ($action == 'add_holiday'){
    $holidays_id = filter_input(INPUT_POST,'holidays_id',FILTER_VALIDATE_INT);
    $description = filter_input(INPUT_POST,'description');
    $date_start = filter_input(INPUT_POST,'date_start');
    $date_end = filter_input(INPUT_POST,'date_end');
    $destination = filter_input(INPUT_POST,'destination');
    $cost = filter_input(INPUT_POST,'cost', FILTER_VALIDATE_FLOAT);
    if ($holidays_id == NULL || $holidays_id == FALSE || $description == NULL || $date_start == NULL || $date_end == NULL || $destination == NULL || $cost == NULL || $cost == FALSE){
        $error = "Invalid holiday data. Check all firlds and try again.";
        include('./errors/error.php');
    } else{
        require('./model/database.php');

       // $query = 'INSERT INTO holidays(holidays_id,description,date_start,date_end,destination) VALUES (:holidays_id,:description,:date_start,:date_end,:destination)';
       // $statement = $db->prepare($query);
       // $statement->bindValue(':holidays_id',$holidays_id);
        //$statement->bindValue(':description',$description);
        //$statement->bindValue(':date_start',$date_start);
        //$statement->bindValue(':date_end',$date_end);
        //$statement->bindValue(':destination',$destination);
        //$statement->execute();
        //$statement->closeCursor();
       // include(holiday_add.php);
        add_holiday($holidays_id,$description,$date_start,$date_end,$destination,$cost);
        header("Location: .?holidays_id=$holidays_id");
    } 
}
include_once 'dbconfig.php';
?>


