<?php

function get_holidays(){
    global $db;
    $query = 'SELECT * FROM holidays';
    $statement = $db->prepare($query);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}
function get_holidays_id($holidays_id){
    global $db;
    $query = 'SELECT * from holidays WHERE holidays_id = :holidays_id';
    $statement = $db->prepare($query);
    $statement->bindValue(":holidays_id",$holidays_id);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}
function get_description($description){
    global $db;
    $query = 'SELECT * from holidays WHERE description = :description';
    $statement = $db->prepare($query);
    $statement->bindValue(":description",$description);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}
function get_date_start($date_start){
    global $db;
    $query = 'SELECT * FROM holidays WHERE date_start = :date_start';
    $statement = $db->prepare($query);
    $statement->bindValue(":date_start",$date_start);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}
function get_date_end($date_end){
    global $db;
    $query = 'SELECT * FROM holidays WHERE date_end = :date_end';
    $statement = $db->prepare($query);
    $statement->bindValue(":date_end",$date_end);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}
function get_destination($destination){
    global $db;
    $query = 'SELECT * from holidays WHERE destination= :destination';
    $statement = $db->prepare($query);
    $statement->bindValue(":destination",$destination);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}
function get_cost($cost){
    global $db;
    $query = 'SELECT * from holidays WHERE cost= :cost';
    $statement = $db->prepare($query);
    $statement->bindValue(":cost",$cost);
    $statement->execute();
    $holidays = $statement->fetchAll();
    $statement->closeCursor();
    return $holidays;
}

function delete_holiday($holidays_id){
    global $db;
    $query = 'DELETE FROM holidays WHERE Holidays_id = :holidays_id';
    $statement = $db->prepare($query);
    $statement->bindValue(":holidays_id",$holidays_id);
    $statement->execute();
    $statement->closeCursor();
}

function add_holiday($holidays_id,$description,$date_start,$date_end,$destination,$cost){
    global $db;
    $query = 'INSERT INTO holidays(holidays_id,description,date_start,date_end,destination,cost) VALUES (:holidays_id,:description,:date_start,:date_end,:destination,:cost)';
    $statement = $db->prepare($query);
    $statement->bindValue(':holidays_id',$holidays_id);
    $statement->bindValue(':description',$description);
    $statement->bindValue(':date_start',$date_start);
    $statement->bindValue(':date_end',$date_end);
    $statement->bindValue(':destination',$destination);
    $statement->bindValue(':cost',$cost);
    $statement->execute();
    //$holidays = $statement->fetchAll();
    $statement->closeCursor();
   //return holidays;
}

?>

