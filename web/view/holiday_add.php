<?php include './view/header.php';?>
<main>
    <h1>Add Holiday</h1>
    <form action="index.php" method="post" id="add_holiday_form">
        <input type="hidden" name="action" value="add_holiday">
        
        <label>Holiday_id:</label>
        <input type="text" name="holidays_id"><br>
        <label>Description:</label>
        <input type="text" name="description"><br>
        <label>Date_Start:</label>
        <input type="text" name="date_start"><br>
        <label>Date_End:</label>
        <input type="text" name="date_end"><br>
        <label>Destination:</label>
        <input type="text" name="destination"><br>
        <label>Cost:</label>
        <input type="text" name="cost"><br>
        
        <label>&nbsp;</label>
        <input type="submit" value="Add Holiday" />
    </form>
    <p><a href="index.php?action=list_holidays">View Holiday List</a></p>
</main>
<?php include './view/footer.php'; ?>
